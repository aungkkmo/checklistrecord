<?php
	// require 'confs/auth.php';
	// session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta chatsert="Shift-JIS">
	 <link rel="icon" type="image/png" href="img/767175.png">
	<!-- <link rel="stylesheet" type="text/css" href="css/style.css"> -->
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/reset.css"> -->

	<style type="text/css">
		table th{
			text-align: center;
		}
		th , td {
			font-size: 12px;
			padding: 0px;
			margin:0px;
		}
	/*	.hide {
		display: none;
		}*/
		tbody tr:hover {
			cursor: default;
			
		}
		tbody tr a:hover {
			cursor:pointer;
		}
		.remark {
			color:#f00;
		}
	/*	footer {
			height: 100px;
		}*/
		footer .container {
			padding-top: 20px;

		}
		footer .container p{
			text-align: center;
		}
		.pagi {
			text-align: center;
			position: relative;
		}
		#cpright {
			text-align: center;
		}
		</style>

</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">QA-TPL のプロジェっト</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
   

      <ul class="nav navbar-nav navbar-right">       
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['user']; ?><span class="caret"></span></a>
          <ul class="dropdown-menu" >
            <li><a href="administration.php">Admin Panel</a></li>            
            <li><a href="logout.php">Log Out</a></li>    
          
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



	<table class="table table-hover table-responsive table-bordered">
		<thead>
		<tr>	
				<th>ID</th>
				<th>案件番号</th>
				<th>お客様名</th>
				<th>受注日</th>
				<th>ご要望納期</th>
				<th>納品日</th>		
				<th>テスト対象URL</th>
				<th>Remark</th>
				<th colspan="2">Option</th>

		</tr>
			
		</thead>
		<tbody>
		<?php 
		include 'confs/dbconfig.php';		
			 
	        $query = "SELECT * FROM tpltb";       
			$records_per_page=10;
			$newquery = $paginate->paging($query,$records_per_page);
			$paginate->dataview($newquery);
					
		
		?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="10" id="cpright">&copy; Copyright by TeamQA</td>
				
		</tfoot>
	</table>
	<nav class="pagi">
  <ul class="pagination">
  
    <?php
    	$paginate->paginglink($query,$records_per_page);
    ?>
   
  </ul>
</nav>


<script src="bower_components/jquery/dist/jquery.js"></script>
<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#edit').click(function(){
			$('.edit').toggle('hide');
		});
		$('#hideBack').click(function(){
			$('.edit').toggle('hide');
		});
			

	});
</script>
<script type="text/javascript">
		function delete_id(id){
		if(confirm('Sure To Remove This Record ' + id + '?')){
        window.location.href='delete.php?id='+id;


     }
	}
</script>
</body>
</html>
