
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
     <link rel="icon" type="image/png" href="img/767175.png">
     <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
    <div>
      <form action="insert-register.php" method="post">
        <fieldset>
        <legend><img src="img/qualy.svg"/><span class="number">Register</span></legend>

          <input type="text" name="name" placeholder="User Name *" required>
          <input type="password" name="password" id="pass1" placeholder="Password *" required>
          <input type="password" name="confirm_password" id="pass2" placeholder="Confirm password *" onkeyup="checkPass(); return false;" required>
          <span id="confirmMessage" class="confirmMessage"></span>
          <input type="email" name="email" placeholder="Email *" >


        </fieldset>
          <input type="submit" value="Apply" />
      </form>

    </div>
    <script type="text/javascript">
    function checkPass()
    {
      //Store the password field objects into variables ...
      var pass1 = document.getElementById('pass1');
      var pass2 = document.getElementById('pass2');
      //Store the Confimation Message Object ...
      var message = document.getElementById('confirmMessage');
      //Set the colors we will be using ...
      var goodColor = "#66cc66";
      var badColor = "#ff6666";
      //Compare the values in the password field
      //and the confirmation field
      if(pass1.value == pass2.value){
          //The passwords match.
          //Set the color to the good color and inform
          //the user that they have entered the correct password
          pass2.style.backgroundColor = goodColor;
          message.style.color = goodColor;
          message.innerHTML = "Passwords Match!"
      }else{
          //The passwords do not match.
          //Set the color to the bad color and
          //notify the user.
          pass2.style.backgroundColor = badColor;
          message.style.color = badColor;
          message.innerHTML = "Passwords Do Not Match!"
      }
    }
    </script>
  </body>
</html>
