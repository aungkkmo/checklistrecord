Team QA DBMS
===============

QA-TPL Projects' Database
- Date Picker only compatible with Google Chrome


System Specification
Login 
  -No Blank Username or Password

Login Submit 
  - if wrong username
	- javascript alertbox;
  - if right 
	- show table
Click Add
  - Add Form
Click Delete
  - Javascript Confirm
 	if Cancel
		-Do nothing
	if OK 
		-Record must be deleted
   -ID must be replace the deleted one

Two Types of user
	- Normal
	- Administrator

 
	*Only Administrator can add new user

For Normal User Role ( * In Progress)
	-View
	-Add	
	-Logout
    -Register(* Admin Approval Required)
For Administrator Role(* In Progress)
	-View
	-Edit
	-Delete
	-Add User
	-Log out
	-Change User Role
	-View users
    -Approve User
