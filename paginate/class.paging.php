<?php

class paginate
{
	private $db;
	
	function __construct($DB_con)
	{
		$this->db = $DB_con;
	}
	
	public function dataview($query)
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
	
		if($stmt->rowCount()>0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				?>
                <tr>
            	<td><?php echo $row['id'] ?></td>
				<td><?php echo $row['proposalNumber'] ?></td>
				<td><?php echo $row['customerName'] ?></td>
				<td><?php echo $row['orderDate'] ?></td>
				<td><?php echo $row['requestDeliveryTime'] ?></td>
				<td><?php echo $row['deliveryDate'] ?></td>
				<td><a href="<?php echo $row['testTargetURL'] ?>" target="_blank" title="Go to <?php echo $row['customerName']  ?>'s site?"><?php echo $row['testTargetURL'] ?></a></td>
				<td class="remark"><?php echo $row['remark'] ?></td>
				<td><a href="edit.php?id=<?php echo $row['id'] ?>" title="Edit Description"><span class="glyphicon glyphicon-pencil"></span></a></td>
				<td><a href="javascript:delete_id(<?php echo $row['id']; ?>)" title="Delete Row"><span class="glyphicon glyphicon-trash"></span></a></td>
				</tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}
		
	}
	
	public function paging($query,$records_per_page)
	{
		$starting_position=0;
		if(isset($_GET["page_no"]))
		{
			$starting_position=($_GET["page_no"]-1)*$records_per_page;
		}
		$query2=$query." limit $starting_position,$records_per_page";
		return $query2;
	}
	
	public function paginglink($query,$records_per_page)
	{
		
		$self = $_SERVER['PHP_SELF'];
		
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		
		$total_no_of_records = $stmt->rowCount();
		
		if($total_no_of_records > 0)
		{
			?><tr><td colspan="3"><?php
			$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
			$current_page=1;
			if(isset($_GET["page_no"]))
			{
				$current_page=$_GET["page_no"];
			}
			if($current_page!=1)
			{
				$previous =$current_page-1;
				
				echo "<a href='".$self."?page_no=".$previous."'>&laquo;</a>&nbsp;&nbsp;";
			}
			for($i=1;$i<=$total_no_of_pages;$i++)
			{
				if($i==$current_page)
				{
					echo "<strong><a href='".$self."?page_no=".$i."' style='color:red;text-decoration:none'>".$i."</a></strong>&nbsp;&nbsp;";
				}
				else
				{
					echo "<a href='".$self."?page_no=".$i."'>".$i."</a>&nbsp;&nbsp;";
				}
			}
			if($current_page!=$total_no_of_pages)
			{
				$next=$current_page+1;
				echo "<a href='".$self."?page_no=".$next."'>&raquo;</a>&nbsp;&nbsp;";
				
			}
			?></td></tr><?php
		}
	}
}
?>

<!-- 	<nav>
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul> -->
</nav>