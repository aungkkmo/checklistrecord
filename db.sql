-- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: teamqadb
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB-1ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tpltb`
--

DROP TABLE IF EXISTS `tpltb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpltb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposalNumber` int(11) NOT NULL,
  `customerName` varchar(255) DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `requestDeliveryTime` date DEFAULT NULL,
  `deliveryDate` date DEFAULT NULL,
  `testTargetURL` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tpltb`
--

LOCK TABLES `tpltb` WRITE;
/*!40000 ALTER TABLE `tpltb` DISABLE KEYS */;
INSERT INTO `tpltb` VALUES (1,909,'ティ・スタイルハウス株式会社','2015-07-10','2015-07-10','2015-07-10','http://sktpl24.heteml.jp/t-stylehouse.co.jp/',NULL),(2,916,'有限会社とわ（なかはら接骨院）','2015-07-13','2015-07-13','2015-07-13','http://sktpl24.heteml.jp/musashinakahara-sekkotsuin.com','キャンセル'),(3,928,'Abundance-Moon','2015-07-14','2015-07-14','2015-07-14','http://sktpl24.heteml.jp/abundance-moon.com/',''),(4,905,'サロン ド アトリエ','2015-07-15','2015-07-15','2015-07-15','http://sktpl24.heteml.jp/salon-de-atelier-hikari.com/',NULL),(5,923,'リラクゼーションケアサロン　笹塚','2015-07-16','2015-07-17','2015-07-17','http://sktpl24.heteml.jp/rcs-school.com',NULL),(6,925,'Ｈａｉｒ　シャポー','2015-07-17','2015-07-21','2015-07-20','http://sktpl24.heteml.jp/fukuoka-hairsalon-chapeau.com',''),(7,927,'Yoga Surya Bali','2015-07-21','2015-07-22','2015-10-21','http://sktpl24.heteml.jp/yogasurya.jp',''),(8,940,'Kボイススタジオ','2015-07-22','2015-07-23','2015-07-22','http://sktpl25.heteml.jp/k-voicestudio.com','URL変更有り'),(9,918,'有限会社 とわ（関城接骨院 桜ヶ丘院）','2015-07-23','2015-07-24','2015-07-24','http://sktpl24.heteml.jp/sekishirosakura.com',''),(10,966,'セイビア相模原店','2015-07-27','2015-07-27','2015-07-28','http://sktpl25.heteml.jp/savior3.com','GoogleDrive上の共有設定ミスで納品遅れ'),(11,926,'家づくりのめぐみ建設','2015-07-29','2015-07-30','2015-07-30','http://sktpl25.heteml.jp/megumi-w.com',''),(12,939,'ﾂｲﾝｸﾙ2','2015-07-30','2015-07-30','2015-07-30','http://sktpl25.heteml.jp/twinkle-2.com/',''),(13,831,'BRIM','2015-07-31','2015-08-03','2015-08-03','http://sktpl23.heteml.jp/brim-fukuoka.com',NULL),(14,930,'はやし克整骨院','2015-08-03','2015-08-03','2015-08-03','http://sktpl25.heteml.jp/fukutsu-koutsuujikochiryou.com',NULL),(15,947,'Kitchen D.Factory','2015-08-04','2015-08-04','2015-08-04','http://sktpl25.heteml.jp/kitchen-d-factory.com',NULL),(16,951,'Co\'Co hair works','2015-07-05','2015-07-05','2015-07-05','http://sktpl25.heteml.jp/coco-hairworks.com',NULL),(17,934,'&#26666;&#24335;&#20250;&#31038;&#12288;&#12488;&#12521;&#12473;&#12488;&#12527;&#12540;&#12463;','2015-08-05','2015-08-05','2015-08-05','http://sktpl25.heteml.jp/trust-wk.co.jp',''),(18,922,'&#12510;&#12522;&#12531;&#12525;&#12540;&#12489;','2015-08-06','2015-08-07','2015-08-07','http://sktpl25.heteml.jp/marine-road.jp',''),(19,941,'&#25163;&#12418;&#12415;&#20966;&#12288;&#12377;&#12387;&#12365;&#12426;','2015-08-07','2015-08-10','2015-08-10','http://sktpl25.heteml.jp/temomi-sukkiri.jp',''),(20,938,'&#20117;&#19978;&#37111;&#31246;&#29702;&#22763;&#20107;&#21209;&#25152;','2015-08-10','2015-08-11','2015-08-10','http://sktpl25.heteml.jp/tax-inoue.com/','&#36578;&#35352;&#12511;&#12473;&#12395;&#12424;&#12427;&#12524;&#12509;&#12540;&#12488;&#28431;&#12428;&#26377;&#12426;&#12289;&#20462;&#27491;&#22577;&#21578;&#12434;&#23455;&#26045;'),(21,963,'CA-CHE CA-CHE','2015-08-11','2015-08-12','2015-08-12','http://ca-che-ca-che.com/',''),(22,1003,'&#12367;&#12429;&#12396;&#12414;&#20581;&#24247;&#12459;&#12452;&#12525;&#38498;','2015-08-12','2015-08-13','2015-08-12','http://kuronuma-chiro.jp/',''),(23,967,'&#26377;&#38480;&#20250;&#31038;&#32191;&#36011;&#24037;&#26989;','2015-08-13','2015-08-14','2015-08-14','http://sktpl25.heteml.jp/watanukikougyou.com',''),(24,959,'HAIR GARDEN TSUBOMI','2015-08-13','2015-08-14','2015-08-14','http://h-g-tsubomi.com/',''),(25,971,'&#12388;&#12416;&#12366;','2015-08-14','2015-08-17','2015-08-14','http://sakanakitchen-tumugi.com/',''),(26,958,'BAR&#31481;&#19979;','2015-08-14','2015-08-17','2015-08-17','http://fukuoka-bar.com/',''),(27,979,'&#26757;&#30000;&#23627;','2015-08-17','2015-08-17','2015-08-17','http://umedaya.jp/',''),(28,964,'&#23567;&#26009;&#29702;&#12480;&#12452;&#12491;&#12531;&#12464;&#26783;','2015-08-17','2015-08-17','2015-08-17','http://dining-fukurou.jp/',''),(29,942,'REVOLUTION','2015-08-18','2015-08-18','2015-08-18','http://hero6273.com/',''),(30,954,'&#12461;&#12515;&#12525;&#12483;&#12488;&#25972;&#39592;&#38498;','2015-08-19','2015-08-20','2015-08-20','http://kumamoto-chiryoin.com/','&#32013;&#21697;&#36933;&#12428;&#65288;&#12372;&#35201;&#26395;&#65306;&#21320;&#21069;&#20013;&#12289;&#23455;&#32318;&#65306;&#21320;&#24460;13:30&#65289;'),(31,950,'&#12450;&#12473;&#12522;&#12540;&#12488;&#12450;&#12525;&#12510;&#12465;&#12450;','2015-08-20','2015-08-21','2015-08-21','http://athlete-aroma-care.com/',''),(32,981,'&#26092;&#24425;&#25151;&#12288;&#24736;','2015-08-21','2015-08-21','2015-08-21','http://shunsaibou-yuu.com/','&#21463;&#27880;&#26178;&#20132;&#28169;&#12395;&#12424;&#12426;&#32013;&#26399;&#22793;&#26356;&#65288;&#21320;&#21069;&#20013;&#8594;&#21320;&#24460;13:00&#65289;'),(33,984,'Pier Believe','2015-08-24','2015-08-25','2015-08-24','http://sktpl26.heteml.jp/garbera.net/',''),(34,993,'&#26666;&#24335;&#20250;&#31038;&#12288;&#12467;&#12511;&#12517;&#12540;&#12474;&#12472;&#12515;&#12497;&#12531;','2015-08-25','2015-08-25','2015-08-25','http://comusejapan.com/',''),(35,998,'&#26377;&#38480;&#20250;&#31038;&#12288;&#12513;&#12487;&#12451;&#12475;&#12501;','2015-08-26','2015-08-26','2015-08-26','http://sktpl26.heteml.jp/the-royal-blue.com',''),(36,1005,'&#26666;&#24335;&#20250;&#31038;&#12288;&#12450;&#12452;&#12524;&#12507;&#12540;&#12512;&#29066;&#26412;','2015-08-26','2015-08-26','2015-08-26','http://ailehome.net/',''),(37,1007,'&#21106;&#28921;&#12420;&#12414;&#12392;','2015-08-26','2015-08-27','2015-08-27','http://kurume-yamato.com/',''),(38,995,'Hair Studio&#12288;ecoH','2015-08-26','2015-08-27','2015-08-27','http://hairstudio-ecoh.com/',''),(39,1015,'&#26666;&#24335;&#20250;&#31038;METS','2015-08-27','2015-08-28','2015-08-28','http://mets-t.jp/',''),(40,1021,'&#37444;&#26495;DINER','2015-08-27','2015-08-28','2015-08-28','http://teppandiner.com/',''),(41,983,'CABLECAR','2015-08-28','2015-08-28','2015-08-28','http://bar-cablecar.com/',''),(42,1000,'&#12415;&#12420;&#12374;&#12431;&#25509;&#39592;&#38498;','2015-08-28','2015-08-31','2015-08-31','http://miya-sekkotsu.com/',''),(43,1008,'Set You Free','2015-08-28','2015-08-31','2015-08-31','http://setyoufree.jp/',''),(44,988,'VITA CAFE&#65286;DELI','2015-08-28','2015-08-31','2015-08-31','http://vitacafeanddeli.com/',''),(45,965,'VIRGO','2015-08-28','2015-09-01','2015-08-31','http://kokura-hairsalon.com/',''),(46,1013,'&#12463;&#12452;&#12540;&#12531;&#12474;&#12473;&#12497;','2015-08-31','2015-09-01','2015-09-01','http://queens-spa-lamp.com/',''),(47,318,'&#12354;&#12414;&#12398;&#25972;&#34899;&#38498;','2015-08-31','2015-09-01','2015-09-01','http://sktpl06.heteml.jp/renewal_amanoseitai.com',''),(48,1026,'&#26377;&#38480;&#20250;&#31038;&#12392;&#12431;(&#32190;&#28716;&#12521;&#12452;&#12474;&#12514;&#12540;&#12523;&#25509;&#39592;&#38498;)','2015-08-31','2015-09-01','2015-09-01','http://ayase-risemall.com/',''),(49,1029,'&#35282;&#22769;','2015-09-01','2015-09-02','2015-09-01','http://kadoichi.jp/',''),(50,1001,'&#26377;&#38480;&#20250;&#31038; &#39640;&#23713;&#29983;&#27963;&#20581;&#24247;&#21307;&#30274;&#25903;&#25588;&#12469;&#12540;&#12499;&#12473;','2015-09-02','2015-09-02','2015-09-02','http://takaoka-iryou.com/',''),(51,985,'K&#24111;&#22530;','2015-09-02','2015-09-02','2015-09-02','http://k-taido.com/',''),(52,1020,'URBAN&#12288;CHICKS','2015-09-02','2015-09-03','2015-09-02','http://urbanchicks.jp/',''),(53,1037,'&#30002;&#26000;&#12398;&#22269;','2015-09-02','2015-09-03','2015-09-02','http://kainokuni.com/',''),(54,1028,'&#12508;&#12540;&#12472;&#12517;','2015-09-02','2015-09-03','2015-09-03','http://vosges.jp/',''),(55,999,'La ccord','2015-09-02','2015-09-03','2015-09-03','http://la-ccord.net/',''),(56,968,'&#35199;&#20013;&#27954;&#25972;&#20307;&#38498;','2015-09-03','2015-09-04','2015-09-04','http://sktpl27.heteml.jp/karada.help','定時後納品'),(57,1011,'FELICE&#65374;nuovo&#65374;','2015-09-03','2015-09-04','2015-09-04','http://felice-nuovo.jp/','&#23450;&#26178;&#24460;&#32013;&#21697;'),(58,922,'&#12373;&#12392;&#12358;&#12289;','2015-09-03','2015-09-04','2015-09-04','http://sktpl27.heteml.jp/shirokanesatou.com/','&#23450;&#26178;&#24460;&#32013;&#21697;'),(59,1023,'hair salon LAZO','2015-09-04','2015-09-07','2015-09-07','http://hairsalon-lazo.com/',''),(60,1022,'FME','2015-09-04','2015-09-07','2015-09-07','http://sktpl27.heteml.jp/facial-make-eyelash.jp',''),(61,1010,'&#12354;&#12426;&#12435;&#12371;','2015-09-04','2015-09-07','2015-09-07','http://hairsalon-arinko.com/',''),(62,1043,'&#28988;&#12365;&#40165; &#12395;.comi','2015-09-07','2015-09-08','2015-09-07','http://yakitorinicomi.com/',''),(63,1009,'Shot Bar BATTERY','2015-09-07','2015-09-08','2015-09-07','http://barbattery.com/',''),(64,1030,'co creation PALE YELLOW&#12398;&#22320;&#29699;','2015-09-08','2015-09-09','2015-09-08','http://counseling-oita.jp/',''),(65,1017,'&#12399;&#12363;&#12383;&#23621;&#37202;&#23627; &#12400;&#12363;&#12385;&#12435;','2015-09-08','2015-09-09','2015-09-08','http://fukuokaizakaya.com/',''),(66,1012,'&#34030;&#35211;&#25972;&#39592;&#38498;','2015-09-09','2015-09-10','2015-09-09','http://hasumiseikotuin.com/',''),(67,778,'&#26666;&#24335;&#20250;&#31038;&#12461;&#12515;&#12473;&#12486;&#12451;&#12531;&#12464;&#12490;&#12452;&#12531;','2015-09-10','2015-09-11','2015-09-11','http://sktpl22.heteml.jp/c-t-n.jp',''),(68,1042,'&#26666;&#24335;&#20250;&#31038;&#12456;&#12512;&#12456;&#12473;&#12503;&#12521;&#12531;','2015-09-11','2015-09-14','2015-09-14','http://bistrocoeur.com/',''),(69,1051,'&#26666;&#24335;&#20250;&#31038;A-NEX','2015-09-14','2015-09-15','2015-09-15','http://kamakura-yamakita.com/',''),(70,1056,'MINO\'ake','2015-09-11','2015-09-14','2015-09-15','http://sktpl27.heteml.jp/minoake.com/','チェックシート上書きにより納品遅れ。'),(71,1031,'35 sacko','2015-09-15','2015-09-15','2015-09-15','http://35sacko.com/',NULL),(72,1019,'Joy Joy &#12473;&#12486;&#12540;&#12471;&#12519;&#12531;','2015-09-15','2015-09-16','2015-09-16','http://sktpl27.heteml.jp/joyjoy-sta.com/',NULL),(73,1059,'&#12377;&#12356;&#26376;','2015-09-15','2015-09-16','2015-09-16','http://suigetsu-company.com/',NULL),(74,957,'&#26481;&#20140;&#12522;&#12488;&#12522;&#12488;','2015-09-16','2015-09-17','2015-09-17','http://tokyoritorito.com/',NULL),(75,1025,'SouriRe','2015-09-16','2015-09-17','2015-09-17','http://sourire2015.jp/',NULL),(76,994,'Dining coin bar COLOR','2015-09-17','2015-09-17','2015-09-17','http://bill-color.com/',NULL),(77,1032,'&#12354;&#12373;&#12402;&#25972;&#39592;&#38498;','2015-09-17','2015-09-18','2015-09-18','http://asahi-alba.com/',NULL),(78,1055,'&#25972;&#20307;&#30274;&#34899;&#38498; &#33258;&#20998;&#31354;&#38291; &#12300;&#25105;&#12301; WAGA','2015-09-18','2015-09-18','2015-09-18','http://oomura-seitai.com/',NULL),(79,1054,'lulu malu','2015-09-18','2015-09-24','2015-09-23','http://sktpl27.heteml.jp/fukuoka-biyoushitsu.com',NULL),(80,1044,'Sherry and Rum Bar Lustau','2015-09-18','2015-09-24','2015-09-23','http://sandrlustau.com/',''),(81,1034,'NEXT STYLE','2015-09-24','2015-09-25','2015-09-24','http://next-style.jp/',''),(82,1041,'MANDARIN MARKET &#25991;&#33775;&#24066;&#22580;','2015-09-24','2015-09-25','2015-09-25','http://fukuokachinesecuisine.com/',''),(83,1027,'&#26666;&#24335;&#20250;&#31038; SOPHIA','2015-09-28','2015-09-29','2015-09-29','http://sophia0706.com/',''),(84,1050,'Liberty','2015-09-28','2015-09-29','2015-09-29','http://fukuokahairsalon.com/',''),(85,1058,'&#23447;&#25945;&#27861;&#20154;&#12288;&#40845;&#20307;&#23665;&#20462;&#39443;&#30495;&#35328;&#23447;&#12288;&#40845;&#28304;&#23546;','2015-09-28','2015-09-29','2015-09-29','http://oita-kingandera.com/',''),(86,1057,'&#35914;&#29577;&#21271;&#65300;&#19969;&#30446;&#25509;&#39592;&#38498;','2015-09-29','2015-09-30','2015-09-30','http://toyokita-4.com/',''),(87,1036,'&#12501;&#12457;&#12540;&#12459;&#12473;&#12514;&#12540;&#12499;&#12523;','2015-09-29','2015-09-30','2015-09-30','http://focusmobile.jp/',''),(88,1039,'&#12510;&#12523;&#12479;&#29987;&#26989; &#26666;&#24335;&#20250;&#31038;','2015-09-29','2015-09-30','2015-09-30','http://sktpl27.heteml.jp/grace-garden.jp',''),(89,977,'&#26408;&#21407;&#31246;&#29702;&#22763;&#20107;&#21209;&#25152;','2015-09-29','2015-09-30','2015-09-30','http://kiharazeirishi.com/','');
/*!40000 ALTER TABLE `tpltb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usertb`
--

DROP TABLE IF EXISTS `usertb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertb` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(255) NOT NULL,
  `u_password` varchar(255) NOT NULL,
  `u_email` varchar(255) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usertb`
--

LOCK TABLES `usertb` WRITE;
/*!40000 ALTER TABLE `usertb` DISABLE KEYS */;
INSERT INTO `usertb` VALUES (1,'admin','password','admin@gmail.com');
/*!40000 ALTER TABLE `usertb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-09 13:28:10
